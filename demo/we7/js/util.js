var util = {};
/**
	获取querystring中的某个参数值
	@params name 要获取地址中参数中变量名称
*/
util.querystring = function(name){
	var result = location.search.match(new RegExp("[\?\&]" + name+ "=([^\&]+)","i"));
	if (result == null || result.length < 1){
		return "";
	}
	return result[1];
}

/**
	构造微擎地址, 
	@params action 微擎系统中的controller, action, do，格式为 'wxapp/home/navs'
	@params querystring 格式为 {参数名1 : 值1, 参数名2 : 值2}
*/
util.url = function(action, querystring) {
	var app = getApp();
	var url = app.siteInfo.siteroot + '?i=' + app.siteInfo.uniacid + '&t=' + app.siteInfo.multiid + '&';

	if (action) {
		action = action.split('/');
		if (action[0]) {
			url += 'c=' + action[0] + '&';
		}
		if (action[1]) {
			url += 'a=' + action[1] + '&';
		}
		if (action[2]) {
			url += 'do=' + action[2] + '&';
		}
	}
	if (querystring) {
		for (param in querystring) {
			if (param && querystring[param]) {
				url += 'param=' + querystring[param] + '&';
			}
		}
	}
	return url;
}

/**
	显示一个toast弹窗
	注：使用此函数时，请确保页面中包含一个以option.id为前缀的 toast 标签。

	@params app 微信小程序的app对象，一般页面中传入this即可
	@params option toast弹出窗的参数，列表如下：
	{
		id : 'tips', //toast标签的前缀，默认为tips，可以是任何小写英文字母，区分于页面中多个toast
		message : '修改成功', //toast弹出窗中要显示的信息
		callback : function(){} //toast自动消息后调用的回调函数
	}
*/
util.toast = function(app, option) {
	var option = option ? option : {};
	var message = option.message;
	var id = option.id ? option.id : 'tips';
	var data = {};

	data[id + 'Message'] = message;
	data[id + 'Show'] = true;

	app.setData(data);
	app[id + 'Callback'] = function() {
		if (typeof callback == 'function') {
			callback();
		}
		data[id + 'Show'] = false;
		app.setData(data);
	}
}
/**
	二次封装微信wx.request函数、增加交互体全、配置缓存、以及配合微擎格式化返回数据

	@params app 微信小程序的app对象，一般页面中传入this即可
	@params option 弹出参数表，
	{
		url : 同微信,
		data : 同微信,
		header : 同微信,
		method : 同微信,
		success : 同微信,
		fail : 同微信,
		complete : 同微信,

		datakey : 传入时，获取数据后自动以此名称追加数据到app.data中
		cachetime : 缓存周期，在此周期内不重复请求http，默认为30秒，为0时不缓存
	}
*/
util.request = function(app, option) {
	var option = option ? option : {};
	option.cachetime = option.cachetime ? option.cachetime : 30;

	var url = option.url;
	if (url.indexOf('http://') == -1 && url.indexOf('https://') == -1) {
		url = util.url(url);
	}
	if (!url) {
		return false;
	}
	wx.showNavigationBarLoading();
	if (option.cachetime) {
		var md5 = require('md5.js');
		var cachekey = md5(url);
		var cachedata = wx.getStorageSync(cachekey);
		var timestamp = Date.parse(new Date());

		if (cachedata && cachedata.data && cachedata.expire > timestamp) {
			var result = cachedata.data;
			if (option.success && typeof option.success == 'function') {
				option.success(result);
			}
			console.log('cache:' + url);
			if (option.datakey) {
				var data = {};
				data[option.datakey] = result;
				app.setData(data);
			}
			wx.hideNavigationBarLoading();
			return true;
		}
	}
	wx.request({
		'url' : url,
		'data' : option.data ? option.data : {},
		'header' : option.header ? option.header : {},
		'method' : option.method ? option.method : 'GET',
		'success' : function(response) {
			if (!response.data) {
				util.toast(app, {
					'id' : 'tips',
					'message' : '获取失败'
				});
			} else if (response.data.message.errno) {
				util.toast(app, {
					'id' : 'tips',
					'message' : response.data.message.message
				});
			} else {
				var result = response.data.message.message;
				if (option.success && typeof option.success == 'function') {
					option.success(result);
				}
				if (option.datakey) {
					var data = {};
					data[option.datakey] = result;
					app.setData(data);
				}
				//写入缓存，减少HTTP请求，并且如果网络异常可以读取缓存数据
				if (option.cachetime) {
					var cachedata = {'data' : result, 'expire' : timestamp + option.cachetime * 1000};
					wx.setStorageSync(cachekey, cachedata);
				}
			}
		},
		'fail' : function() {
			if (option.fail && typeof option.fail == 'function') {
				option.fail();
			}
			//如果请求失败，尝试从缓存中读取数据
			var md5 = require('md5.js');
			var cachekey = md5(url);
			var cachedata = wx.getStorageSync(cachekey);
			if (cachedata && cachedata.data) {
				var result = cachedata.data;
				if (option.success && typeof option.success == 'function') {
					option.success(result);
				}
				console.log('failreadcache:' + url);
				if (option.datakey) {
					var data = {};
					data[option.datakey] = result;
					app.setData(data);
				}
				wx.hideNavigationBarLoading();
				return true;
			}
		},
		'complete' : function() {
			wx.hideNavigationBarLoading();
			if (option.complete && typeof option.complete == 'function') {
				option.complete();
			}
		}
	});
}

util.formatTime = function(time) {
	if (typeof time !== 'number' || time < 0) {
		return time;
	}
	var hour = parseInt(time / 3600);
	time = time % 3600;
	var minute = parseInt(time / 60);
	time = time % 60;
	var second = time;
	return ([hour, minute, second]).map(function (n) {
		n = n.toString();
		return n[1] ? n : '0' + n;
	}).join(':');
}


module.exports = util;
