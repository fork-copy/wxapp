//index.js
var util = require('../../js/util.js')
//获取应用实例
var app = getApp()
Page({
	data: {
		navs : [],
		slide : [],
		commend : [],
		userInfo: {}
	},
	onLoad: function () {
		var that = this
		//调用应用实例的方法获取全局数据
		app.getUserInfo(function(userInfo){
			//更新数据
			that.setData({
				userInfo:userInfo
			});
		});
		var $this = this;
		//初始化导航数据
		util.request($this, {
			'url' : 'wxapp/home/nav',
			'datakey' : 'navs',
			'cachetime' : '30'
		});
		util.request($this, {
			'url' : 'wxapp/home/slide',
			'datakey' : 'slide',
			'cachetime' : '30'
		});
		util.request($this, {
			'url' : 'wxapp/home/commend',
			'datakey' : 'commend',
			'cachetime' : '30'
		});
	}
});
